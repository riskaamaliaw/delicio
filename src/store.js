import Vue from 'vue'
import Vuex from 'vuex'
import resep from './module/resep'
import getters from './getters'
Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    resep
  },
  getters
})

export default store
