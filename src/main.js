import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vuetify from 'vuetify'
import './registerServiceWorker'
import 'vuetify/dist/vuetify.min.css'
import Firebase from 'firebase/app'
import 'firebase/firestore'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
Vue.config.productionTip = false
Vue.use(Vuetify)
const config = {
    databaseURL: 'https://delicio-a6750.firebaseio.com/',
    apiKey: 'AIzaSyCWbnkvMgz3ZAKG38_0bsF-UAftzLinpwU',
    storageBucket: 'gs://delicio-a6750.appspot.com',
    projectId: 'delicio-a6750'
}
Firebase.initializeApp(config)
const Firestore = Firebase.firestore()
const settings = { timestampsInSnapshots: true }
Firestore.settings(settings)
Vue.prototype.$firestore = Firestore
var openRequest
if ('indexedDB' in window) {
  openRequest = indexedDB.open('deliciodb', 1)
}
openRequest.onupgradeneeded = function (e) {
  var thisDB = e.target.result;

  if (!thisDB.objectStoreNames.contains('tblresep')) {
    thisDB.createObjectStore('tblresep');
  }
}
openRequest.onsuccess = function (e) {
  var db = e.target.result;
  var trs = db.transaction(['tblresep'], "readwrite")
  trs.objectStore('tblresep');
}
store.dispatch('getResep')
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
