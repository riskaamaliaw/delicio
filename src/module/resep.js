import Firebase from 'firebase/app'
import 'firebase/firestore'
import { EventBus } from '@/event-bus.js'

function simpan(data, id) {
    var openRequest
    var db
    if ('indexedDB' in window) {
        openRequest = indexedDB.open('deliciodb', 1)
    }
    openRequest.onupgradeneeded = function (e) {
        var thisDB = e.target.result;
        thisDB.deleteObjectStore('tblresep');
        thisDB.createObjectStore('tblresep');

    }
    openRequest.onsuccess = function (e) {
        db = e.target.result;
        var trs = db.transaction(['tblresep'], "readwrite")
        var store = trs.objectStore('tblresep');
        var request = store.put(data, id);
        request.onerror = function () {
            // console.log("Error", e.target.error.name);
            //some type of error handler
        }

        request.onsuccess = function () {
            // console.log("Woot! Did it");
        }
    }
}
const resep = {
    state: {
        resep: []
    },
    mutations: {
        SET_RESEP: (state, resep) => {
            state.resep.push(resep)
        }
    },
    actions: {
        getResep({commit}) {
            return new Promise((resolve, reject) => {
                Firebase.firestore().collection('resep').onSnapshot((query) => {
                    if (!query.empty) {
                        query.forEach((doc) => {
                            commit('SET_RESEP', doc.data())
                            simpan(doc.data(), doc.id)
                        })
                        EventBus.$emit('resep', 'baru')
                        resolve({ status: 'success' })
                    } else {
                        reject({ status: 'kosong' })
                    }
                    
                })
            })
        }
    }
}
export default resep
